module.exports = function(duration) {
	return function(){
		return new Promise(function(resolve, reject){
			setTimeout(function(){
				resolve();
			}, duration)
		});
	};
};

// Usage:

var delayPromise = require("./delay-promise");

doThing()
.then(...)
.then(delayPromise(5000))
.then(...)